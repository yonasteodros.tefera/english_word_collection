
| Word              | Meaning                                                     | Type      |
| ------------------|:-----------------------------------------------------------:| ---------:|
| moonlit lane      |able to be seen because of the light of the moon:                                               | adjective |
| wands             | a long, thin stick or rod                                   | noun      |
| stowed            | pack or store (an object) carefully and neatly in a particular place  | past tense: stowe|
| cloaks             | something that hides, covers, or keeps something else secret  | noun|
| briskly             | walk in  an active, quick, or energetic way.  | adverb|
| bramble             | a prickly scrambling shrub of the rose family, especially a blackberry.  | noun|
| manicured         | well cared for; well-dressed                                | past tense of manicure|
| hedge             | fence                                                       | noun|
| flapped           | (of a bird) move (its wings) up and down when flying or preparing to fly | pasttense of flap|
| blunt             |  someone who speaks in a direct, often rude way or something with a dull edge or point | adjective|
| led off           |  to guide someone or something away                         | phrasal verb|


